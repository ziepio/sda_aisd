class Node(object):
    def __init__(self, val):
        self.val = val
        self.next = None

    def print_value(self):
        print(self.val)

    def set_next_node(self, next_node):
        self.next = next_node

    def change_value(self, new_value):
        self.val = new_value

    def print_all_list(self):
        if self.next is not None:
            print(f"{self.val}, ", end='')
            next_node = self.next
            next_node.print_all_list()
        else:
            print(self.val)


if __name__ == "__main__":
    try:
        print("List operations")
        value = 5
        n = Node(value)
        m = Node(12)
        o = Node(15)

        n.set_next_node(m)
        m.set_next_node(o)

        # Tutaj korzystamy tylko z N
        n.print_all_list()
    except:
        print("Sth went wrong!")
