class ExampleClass:
    @staticmethod
    def which_is_bigger(val_1, val_2):
        if val_1 > val_2:
            print("Val_1 is higher!")
        elif val_2 > val_1:
            print("Val_2 is higher!")
        else:
            print("Equal!")


if __name__ == "__main__":
    # http://pythontutor.com/live.html
    MyObject = ExampleClass()
    try:
        num1 = input('Enter first number: ')
        num2 = input('Enter second number: ')
        MyObject.which_is_bigger(num1, num2)
    except:
        print("Wrong input!")
